local S = minetest.get_translator("block_league")



local ball = {
  initial_properties = {
    physical = true,
    pointable = false,
    collide_with_objects = false,
    visual = "mesh",
    mesh = "bl_ball.b3d",
    visual_size = {x = 5.0, y = 5.0, z = 5.0},
    collisionbox = {-0.5, -0.5, -0.5, 0.5, 0.8, 0.5},
    use_texture_alpha = true,
    textures = {"bl_ball_unclaimed.png"},
    static_save = false,
  },

  p_name = nil,
  timer = 0, -- solo per farla beccare da block_league.get_ball

  on_activate = function(self, dtime)
    local ball_obj = self.object
    ball_obj:set_animation({x=0,y=40}, 20, 0, true)

    minetest.add_particlespawner({
      attached = ball_obj,
      amount = 10,
      time = 0,
      minpos = {x=0, y=3, z=0},
      maxpos = {x=0, y=3, z=0},
      minvel = vector.multiply({x= 0, y = 1, z = 0}, 30),
      maxvel = vector.multiply({x= 0, y = 1, z = 0}, 30),
      minsize = 20,
      maxsize = 20,
      vertical = true,
      texture = "bl_ball_ray.png"
    })
  end,

  on_step = function(self, d_time, moveresult)
    --se nessuno la sta portando a spasso...
    if self.p_name == nil then
      local pos = self.object:get_pos()
      local objects = minetest.get_objects_inside_radius(pos, 1.5)

      -- se nel suo raggio trova un giocatore in vita, si attacca
      for _, object in pairs(objects) do
        if object:is_player() and object:get_hp() > 0 and arena_lib.is_player_in_arena(object:get_player_name(), "block_league") then
          self:attach(object)
          bl_tutorial.complete_phase5(object:get_player_name())
          return
        end
      end
    end
  end,
}


function ball:attach(player)
  local p_name = player:get_player_name()
  local arena = arena_lib.get_arena_by_player(p_name)

  self.p_name = p_name

  minetest.sound_play("bl_crowd_cheer", {to_player = p_name})
  block_league.HUD_ball_update(p_name, S("You've got the ball!"), "0xabf877")

  player:get_meta():set_int("bl_has_ball", 1)
  block_league.stamina_drain(arena, p_name)

  local ball_obj = self.object

  ball_obj:set_attach(player, "Body", {x=0, y=18, z=0}, {x=0, y=0, z=0})
  ball_obj:set_properties({textures={"bl_ball_blue.png"}})
  ball_obj:set_animation({x=120,y=160}, 20, 0, true)   -- smette di oscillare quando presa
end


function ball:reset()
  local p_name = self.p_name

  -- annuncio
  minetest.sound_play("bl_voice_ball_reset", {to_player = p_name})
  block_league.HUD_ball_update(p_name, S("Ball reset"))

  local wielder = minetest.get_player_by_name(p_name)
  local ball_obj = self.object

  self.p_name = nil
  wielder:set_physics_override({speed = block_league.SPEED})
  wielder:get_meta():set_int("bl_has_ball", 0)
  ball_obj:set_detach()
  ball_obj:set_properties({textures={"bl_ball_unclaimed.png"}})
  ball_obj:set_animation({x=0,y=40}, 20, 0, true)

  bl_tutorial.reset_ball(ball_obj, p_name) -- azioni extra del tutorial
end

minetest.register_entity("bl_tutorial:ball", ball)