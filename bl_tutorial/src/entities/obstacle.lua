local multiplier = 4
local obstacle = {
  initial_properties = {
  physical = true,
  collide_with_objects = true,
  visual = "mesh",
  mesh = "bl_tutorial_obstacle.obj",
  backface_culling = false,
  visual_size = {x = 1 * multiplier, y = 1 * multiplier, z = 1 * multiplier},
  textures = {"bl_tutorial_obstacle.png"},
  collisionbox = {-0.35 * multiplier, -0.5 * multiplier, -0.22 * multiplier, 0.35 * multiplier, 0.5 * multiplier, 0.22 * multiplier}
  },

  on_death = function(self, killer)
    local p_name = killer:get_player_name()
    if not arena_lib.is_player_in_arena(p_name) then return end

    minetest.sound_play("bl_tutorial_sentry_death", {object = self.object, max_hear_distance = 20})
    bl_tutorial.decrease_obstacles(p_name)
  end,
}

minetest.register_entity("bl_tutorial:obstacle", obstacle)