-- TODO metti schematica dentro cartella WE
local function load_schematic()
  local src_dir = minetest.get_modpath("bl_tutorial") .. "/schems"
  local wrld_dir = minetest.get_worldpath() .. "/schems"
  local mod_schem = io.open(src_dir .. "/tutorial_map.we", "r")
  local mod_schem_size = mod_schem:seek("end")
  local wrld_schem = io.open(wrld_dir .. "/tutorial_map.we")

  if not wrld_schem or mod_schem_size ~= wrld_schem:seek("end") then
    minetest.cpdir(src_dir, wrld_dir)

    if wrld_schem then
      io.close(wrld_schem)
      minetest.log("[BL Tutorial] Schematic of the tutorial map was outdated. Updated to the new version")
    end
  end

  io.close(mod_schem)
end


load_schematic()