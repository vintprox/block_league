local function fade() end

local huds = {}                   -- KEY: p_name; VALUE: {HUD}
local FADE_TIME = 1.6
local WHITE_SCREEN = {
  hud_elem_type = "image",
  text      = "bl_tutorial_fade.png",
  scale     = { x = 2000, y = 2000},
  number    = 0xFFFFFF,
  z_index   = 99999
  }



function bl_tutorial.hud_add(p_name)
  local player = minetest.get_player_by_name(p_name)
  local hud = player:hud_add(WHITE_SCREEN)
  huds[p_name] = hud
end



function bl_tutorial.hud_remove(p_name)
  minetest.get_player_by_name(p_name):hud_remove(huds[p_name])
  huds[p_name] = nil
end



function bl_tutorial.hud_show(player)
  local p_name = player:get_player_name()
  local hud_id = huds[p_name]
  local hud = player:hud_get(hud_id)

  player:hud_change(hud_id, "text", "bl_tutorial_fade.png")


  minetest.after(0.3, function()
    if not arena_lib.is_player_in_arena(p_name) then return end
    fade(player, FADE_TIME)
  end)
end





function fade(player, time_left)
  local p_name = player:get_player_name()
  local hud_id = huds[p_name]
  local hud = player:hud_get(hud_id)

  if time_left <= 0 then
    hud.text = ""
    return
  end

  minetest.after(0.1, function()
    if not arena_lib.is_player_in_arena(p_name) then return end

    local time_left = time_left - 0.1
    local opacity = 255 / FADE_TIME * time_left

    player:hud_change(hud_id, "text", "bl_tutorial_fade.png^[opacity:" .. opacity)
    fade(player, time_left)
  end)
end