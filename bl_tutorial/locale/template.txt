# version 0.8.0
# author(s):
# reviewer(s):
# textdomain: bl_tutorial

# nodes.lua
BL Tutorial=
Floor edge @1=
slab=
Light grey floor=
Blue floor=
Wall=
Fence=
Chain-link fence=
Platform=
Platform 1/2=
Goal floor=
Goal pole=
Invisible wall=

# phases.lua
Welcome to Block League! Take a deep breath, and take a look around=
Let's just go for a walk=
Reach the end of the hallway=
Do you see that platform up high? There is no way you can reach it without a propulsor=
Grab the propulsor at the end of the steps=
Now reach the platform!=
LMB on a block to bounce (consumes stamina, yellow bar)=
And now for the cool part: shooting!=
Here's a weapon: have fun with the obstacles in front of you=
LMB/RMB: shoot, Q: reload | Obstacles left: @1=
Excellent! And have you noticed that left click and right click shoot differently?=
Beware! An enemy is appearing!=
Take cover with the propulsor (right click) and then eliminate it from a safe place=
Sentry guns left: @1=
Oh no, they're increasing!=
Reach the next section=
Try again!=
Well done! Now let's see how Touchdown works=
To score... just grab the ball and bring it into the enemy goal post (in front of you)!=
Grab the ball and reach the goal post=
And that's how you score!=
Have you noticed that your stamina decreases when you hold the ball? If it reaches zero, you'll go slower=
Now try to go through the rays of your team colour (you currently have the ball)=
Go through the blue rays=
Exactly: rays of your team colour reset the ball. The other ones will kill you though!=
Alright then. You're now ready to become the best Block Leaguer ever: godspeed!=
Returning to the world in @1...=
