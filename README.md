# Block League

S4 League inspired shooter minigame for Minetest. All main files are in the `block_league` subfolder

<a href="https://liberapay.com/Zughy/"><img src="https://i.imgur.com/4B2PxjP.png" alt="Support my work"/></a>

### Want to help?
Feel free to:
* open an [issue](https://gitlab.com/zughy-friends-minetest/block_league/-/issues)
* submit a merge request. In this case, PLEASE, do follow milestones and my [coding guidelines](https://cryptpad.fr/pad/#/2/pad/view/-l75iHl3x54py20u2Y5OSAX4iruQBdeQXcO7PGTtGew/embed/). I won't merge features for milestones that are different from the upcoming one (if it's declared), nor messy code
* contact me on Matrix, on my server [dev room](https://matrix.to/#/!viLipqDNOHxQJqQRGI:matrix.org)

### License  
If not stated otherwise, images and models are made by Zughy, [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
